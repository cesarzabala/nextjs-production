import React, { ChangeEvent, FC, useState } from 'react';
import { Dialog, TextInput } from 'evergreen-ui';

type Props = {
  onNewDoc: any;
  close: any;
  isShown: boolean;
};

const NewDocDialog: FC<Props> = ({ onNewDoc, close, ...props }) => {
  const [name, setName] = useState('');
  const [saving, setSaving] = useState(false);

  const handleNewDocument = async () => {
    setSaving(true);
    await onNewDoc(name);
    setSaving(false);
    setName('');
    close();
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  return (
    <Dialog
      {...props}
      title="New Document"
      confirmLabel="create"
      intent="success"
      onConfirm={handleNewDocument}
      isConfirmLoading={saving}
      onCancel={close}
      onCloseComplete={close}
    >
      <TextInput value={name} onChange={handleChange} placeholder="doc name" />
    </Dialog>
  );
};

export default NewDocDialog;
