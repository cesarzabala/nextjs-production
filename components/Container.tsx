import { Pane } from 'evergreen-ui';

type Props = {
  children?: React.ReactNode;
  height?: string;
};

const Container = ({ children, ...styles }: Props) => (
  <Pane maxWidth="960px" padding="10px" marginX="auto" width="100%" {...styles}>
    {children}
  </Pane>
);

export default Container;
