import React, { ChangeEvent, FC, useState } from 'react';
import { Dialog, TextInput } from 'evergreen-ui';

type Props = {
  onNewFolder: any;
  close: any;
  isShown: boolean;
};

const NewFolderDialog: FC<Props> = ({ onNewFolder, close, ...props }) => {
  const [name, setName] = useState('');
  const [saving, setSaving] = useState(false);

  const handleNewFolder = async () => {
    setSaving(true);
    await onNewFolder(name);
    setSaving(false);
    setName('');
    close();
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  return (
    <Dialog
      {...props}
      title="New Folder"
      confirmLabel="create"
      intent="success"
      onConfirm={handleNewFolder}
      isConfirmLoading={saving}
      onCancel={close}
      onCloseComplete={close}
    >
      <TextInput value={name} onChange={handleChange} placeholder="folder name" />
    </Dialog>
  );
};

export default NewFolderDialog;
