import { Pane, majorScale, Menu, FolderCloseIcon } from 'evergreen-ui';
import React, { FC } from 'react';
import { useRouter } from 'next/router';

type Props = {
  folders: any[];
};
const FolderList: FC<Props> = ({ folders }) => {
  const router = useRouter();
  return (
    <Pane padding={majorScale(2)}>
      <Menu>
        {folders.map((folder) => (
          <Menu.Item key={folder.idx} icon={FolderCloseIcon} onClick={() => router.push(`/app/${folder.idx}`)}>
            {folder.name}
          </Menu.Item>
        ))}
      </Menu>
    </Pane>
  );
};

FolderList.defaultProps = {
  folders: []
};

export default FolderList;
