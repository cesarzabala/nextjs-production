import React, { FC } from 'react';
import { Icon, PlusIcon, Pane, Tooltip } from 'evergreen-ui';

type Props = {
  onClick: any;
  tooltip?: string;
  size?: number;
};

const NewFolderButton: FC<Props> = ({ onClick, tooltip, size }) => {
  return (
    <Tooltip content={tooltip}>
      <Pane onClick={onClick}>
        <Icon icon={PlusIcon} size={size} cursor="pointer" />
      </Pane>
    </Tooltip>
  );
};

NewFolderButton.defaultProps = {
  tooltip: 'New Folder',
  size: 42
};

export default NewFolderButton;
