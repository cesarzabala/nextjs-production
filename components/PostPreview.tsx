import React, { FC } from 'react';
import { Pane, Heading, Paragraph, Button, majorScale } from 'evergreen-ui';
import Link from 'next/link';

import { Post } from '../types/types';

type Props = {
  post: Post;
};

const PostPreview: FC<Props> = ({ post }) => {
  return (
    <Pane padding={majorScale(2)} border borderRadius={4}>
      <Heading size={700} marginBottom={majorScale(2)}>
        {post.title}
      </Heading>
      <Paragraph marginBottom={majorScale(2)}>{post.summary}</Paragraph>
      <Pane textAlign="right">
        <Link href={`/blog/${post.slug}`}>
          <a>
            <Button appearance="minimal" intent="success" height={48}>
              Read
            </Button>
          </a>
        </Link>
      </Pane>
    </Pane>
  );
};
export default PostPreview;
