import React, { FC, useState } from 'react';
import { Pane, Dialog, majorScale } from 'evergreen-ui';
import { useRouter } from 'next/router';
import Logo from '../../components/Logo';
import FolderList from '../../components/FolderList';
import NewFolderButton from '../../components/NewFolderButton';
import User from '../../components/User';
import FolderPane from '../../components/FolderPane';
import DocPane from '../../components/DocPane';
import NewFolderDialog from '../../components/NewFolderDialog';
import { UserSession } from '../../types/types';
import { GetServerSidePropsContext } from 'next';
import { getSession, useSession } from 'next-auth/react';
import { connectToDB, folder, doc } from '../../lib';

type SessionUser = {
  user: UserSession;
};
type Props = {
  folders: any[];
  activeFolder?: any;
  activeDoc?: any;
  activeDocs?: any;
  session?: SessionUser;
};
type ServerProps = {
  id: Array<string>;
};

const App: FC<Props> = ({ folders, activeDoc, activeFolder, activeDocs }) => {
  const router = useRouter();
  const { data: session, status } = useSession();
  const [newFolderIsShown, setIsShown] = useState(false);
  const [allFolder, setAllFolders] = useState(folders);

  const handleNewFolder = async (name: string) => {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}/api/folder`, {
      method: 'POST',
      body: JSON.stringify({ name }),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    const { data } = await res.json();
    setAllFolders((state) => [...state, data]);
  };

  if (status === 'loading') {
    return null;
  }

  const Page = () => {
    if (activeDoc) {
      return <DocPane folder={activeFolder} doc={activeDoc} />;
    }

    if (activeFolder) {
      return <FolderPane folder={activeFolder} docs={activeDocs} />;
    }

    return null;
  };

  if (!session && status === 'unauthenticated') {
    return (
      <Dialog
        isShown
        title="Session expired"
        confirmLabel="Ok"
        hasCancel={false}
        hasClose={false}
        shouldCloseOnOverlayClick={false}
        shouldCloseOnEscapePress={false}
        onConfirm={() => router.push('/signin')}
      >
        Sign in to continue
      </Dialog>
    );
  }

  return (
    <Pane position="relative">
      <Pane width={300} position="absolute" top={0} left={0} background="tint2" height="100vh" borderRight>
        <Pane padding={majorScale(2)} display="flex" alignItems="center" justifyContent="space-between">
          <Logo />
          <NewFolderButton onClick={() => setIsShown(true)} />
        </Pane>
        <Pane>
          <FolderList folders={allFolder} />
        </Pane>
      </Pane>
      <Pane marginLeft={300} width="calc(100vw - 300px)" height="100vh" overflowY="auto" position="relative">
        <User user={session?.user as UserSession} />
        <Page />
      </Pane>
      <NewFolderDialog close={() => setIsShown(false)} isShown={newFolderIsShown} onNewFolder={handleNewFolder} />
    </Pane>
  );
};

App.defaultProps = {
  folders: []
};

export const getServerSideProps = async (ctx: GetServerSidePropsContext<ServerProps>) => {
  const session = await getSession(ctx);

  if (!session || !session.user) {
    return { props: {} };
  }

  const { db } = await connectToDB();
  const { id: userId } = session.user as any;
  const folders = await folder.getFolders(db, userId);

  let activeFolder = null;
  let activeDocs = null;
  let activeDoc = null;
  if (ctx && ctx.params && ctx.params.id && ctx.params.id.length) {
    const [idActiveFolder, isActiveDoc] = ctx.params.id;
    activeFolder = folders.find((folder) => folder.idx === idActiveFolder);
    activeDocs = await doc.getDocsByFolder(db, activeFolder?.idx);

    if (isActiveDoc) {
      activeDoc = activeDocs.find((doc: any) => doc.idx === isActiveDoc);
    }
  }
  const userSession = session as unknown as SessionUser;
  const props: Props = { folders, activeFolder, activeDocs, activeDoc, session: userSession };

  return { props };
};

export default App;
