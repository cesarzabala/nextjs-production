import { majorScale, Pane } from 'evergreen-ui';
import type { GetStaticPropsContext, NextPage } from 'next';
import Link from 'next/link';
import Container from '../components/Container';
import FeatureSection from '../components/FeatureSection';
import Hero from '../components/Hero';
import HomeNav from '../components/HomeNav';
import { home } from '../content';

type TPost = {
  title: string;
  body: string;
};

type TContent = {
  hero: TPost;
  features: Array<TPost>;
};
interface IHome {
  content: TContent;
}

const Home: NextPage<IHome> = ({ content }) => {
  return (
    <Pane>
      <header>
        <HomeNav />
        <Container>
          <Hero content={content.hero} />
        </Container>
      </header>
      <main>
        {content.features.map((feature, i) => (
          <FeatureSection
            key={feature.title}
            title={feature.title}
            body={feature.body}
            image="/docs.png"
            invert={i % 2 === 0}
          />
        ))}
      </main>
      <footer>
        <Pane background="overlay" paddingY={majorScale(9)}>
          <Container>
            Created by{' '}
            <Link href={'https://www.linkedin.com/in/cesarzabala/'}>
              <a target="_blank">Cesar Zabala</a>
            </Link>{' '}
            | Powered by{' '}
            <Link href={'https://nextjs.org/'}>
              <a target="_blank">NextJS</a>
            </Link>
          </Container>
        </Pane>
      </footer>
    </Pane>
  );
};

export const getStaticProps = (ctx: GetStaticPropsContext) => {
  return {
    props: {
      content: ctx.preview ? home.draft : home.published
    }
  };
};

export default Home;
