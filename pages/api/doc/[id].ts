import { NextApiResponse } from 'next';
import nc from 'next-connect';
import middleware from '../../../middleware/all';
import { Request } from '../../../types/types';
import { doc } from '../../../lib';
import onError from '../../../middleware/error';

const handler = nc<Request, NextApiResponse>({
  onError
});

handler.use(middleware);

handler.put(async (req, res) => {
  const updated = await doc.updateOne(req.db, req.query.id as string, req.body);

  res.send({ data: updated });
});

export default handler;
