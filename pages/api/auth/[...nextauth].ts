import { NextApiRequest, NextApiResponse } from 'next';
import NextAuth from 'next-auth/next';
import GithubProvider from 'next-auth/providers/github';
import { doc, folder } from '../../../lib';
import { MongoDBAdapter } from '@next-auth/mongodb-adapter';
import connectToDB, { connectDb } from '../../../lib/mongodb';
import { JWT } from 'next-auth/jwt';

export default (req: NextApiRequest, res: NextApiResponse) =>
  NextAuth(req, res, {
    session: {
      strategy: 'jwt'
    },
    jwt: {
      secret: process.env.JWT_SECRET
    },
    providers: [
      GithubProvider({
        clientId: process.env.GITHUB_ID || '',
        clientSecret: process.env.GITHUB_SECRET || ''
      })
    ],
    adapter: MongoDBAdapter(connectDb()),
    pages: {
      signIn: '/signin'
    },
    callbacks: {
      async session({ session, token }: { session: any; token: JWT }) {
        if (session && session.user && token) {
          session.user.id = token.id;
        }
        return session;
      },
      async jwt({ token, user, isNewUser }) {
        const { db } = await connectToDB();

        if (isNewUser) {
          const personalFolder = await folder.createFolder(db, { createdBy: `${user?.id}`, name: 'Getting Started' });
          await doc.createDoc(db, {
            name: 'Start Here',
            folder: personalFolder?.idx,
            createdBy: `${user?.id}`,
            content: {
              time: 1556098174501,
              blocks: [
                {
                  type: 'header',
                  data: {
                    text: 'Some default content',
                    level: 2
                  }
                }
              ],
              version: '2.12.4'
            }
          });
        }

        if (token && user) {
          return { ...token, id: `${user.id}` };
        }

        return token;
      }
    }
  });
