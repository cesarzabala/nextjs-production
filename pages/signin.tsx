import { Pane, majorScale, Text } from 'evergreen-ui';
import { useRouter } from 'next/router';
import { signIn, useSession } from 'next-auth/react';
import Logo from '../components/Logo';
import SocialButton from '../components/SocialButton';
import { useEffect } from 'react';
export enum Icons {
  GITHUB = 'github',
  GOOGLE = 'google'
}

const Signin = () => {
  const { data: session } = useSession();
  const router = useRouter();

  useEffect(() => {
    if (session) {
      router.push('/app');
    }
  }, [session, router]);

  return (
    <Pane height="100vh" width="100vw" display="flex">
      <Pane
        height="100%"
        width="50%"
        borderRight
        paddingX={majorScale(8)}
        paddingY={majorScale(5)}
        background="#47B881"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Pane>
          <Logo color="white" fontSize="60px" />
          <Pane marginTop={majorScale(2)}>
            <Text color="white" fontSize="22px">
              Sign in.
            </Text>
          </Pane>
        </Pane>
      </Pane>
      <Pane
        height="100%"
        width="50%"
        background="tint2"
        display="flex"
        alignItems="center"
        justifyContent="center"
        paddingX={majorScale(7)}
      >
        <Pane width="100%" textAlign="center">
          <SocialButton type={Icons.GITHUB} onClick={() => signIn('github')} />
        </Pane>
      </Pane>
    </Pane>
  );
};

export default Signin;
