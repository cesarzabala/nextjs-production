import React, { FC } from 'react';
import { serialize } from 'next-mdx-remote/serialize';
import { majorScale, Pane, Heading, Spinner } from 'evergreen-ui';
import Head from 'next/head';
import { useRouter } from 'next/router';
import Container from '../../components/Container';
import HomeNav from '../../components/HomeNav';
import fs from 'fs';
import path from 'path';
import matter from 'gray-matter';
import { posts as postsFromCMS } from '../../content';
import { MDXRemote, MDXRemoteSerializeResult } from 'next-mdx-remote';
import { GetStaticPropsContext } from 'next';
import { PostFrontMatter } from '../../types/types';

type StaticProps = {
  slug: string;
};

interface IPost {
  source: MDXRemoteSerializeResult;
  frontMatter: PostFrontMatter;
}

const BlogPost: FC<IPost> = ({ source, frontMatter }) => {
  const content = source;
  const router = useRouter();

  if (router.isFallback) {
    return (
      <Pane width="100%" height="100%">
        <Spinner size={48} />
      </Pane>
    );
  }
  return (
    <Pane>
      <Head>
        <title>{`Known Blog | ${frontMatter.title}`}</title>
        <meta name="description" content={frontMatter.summary} />
      </Head>
      <header>
        <HomeNav />
      </header>
      <main>
        <Container>
          <Heading fontSize="clamp(2rem, 8vw, 6rem)" lineHeight="clamp(2rem, 8vw, 6rem)" marginY={majorScale(3)}>
            {frontMatter.title}
          </Heading>
          <MDXRemote {...content} components={{ Pane }} />
        </Container>
      </main>
    </Pane>
  );
};

export const getStaticPaths = () => {
  const postsPath = path.join(process.cwd(), 'posts');
  const fileNames = fs.readdirSync(postsPath);
  const slugs = fileNames.map((file) => {
    const fullPath = path.join(process.cwd(), 'posts', file);
    const fileContent = fs.readFileSync(fullPath, 'utf-8');
    const { data } = matter(fileContent);
    return data;
  });

  const fullSlugs = slugs.map((s) => ({ params: { slug: s.slug } }));

  return {
    paths: fullSlugs,
    fallback: true
  };
};

export const getStaticProps = async ({ params, preview }: GetStaticPropsContext<StaticProps>) => {
  console.log('query', params);
  let post = '';
  try {
    const fileName = params?.slug + '.mdx';
    const postsPath = path.join(process.cwd(), 'posts', fileName);
    post = fs.readFileSync(postsPath, 'utf-8');
  } catch (err) {
    const cmsPosts = (preview ? postsFromCMS.draft : postsFromCMS.published).map((post) => {
      return matter(post);
    });
    const match = cmsPosts.find((post) => post.data.slug === params?.slug);
    post = match?.content as string;
  }

  const { data } = matter(post);
  const mdxSource = await serialize(post, { scope: data });

  return {
    props: {
      source: mdxSource,
      frontMatter: data
    }
  };
};

export default BlogPost;
