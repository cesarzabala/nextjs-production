import { FC } from 'react';
import { Pane, majorScale } from 'evergreen-ui';
import matter from 'gray-matter';
import path from 'path';
import fs from 'fs';
//import orderby from 'lodash.orderby';
import Container from '../../components/Container';
import HomeNav from '../../components/HomeNav';
import PostPreview from '../../components/PostPreview';
import { posts as postsFromCMS } from '../../content';
import { Post } from '../../types/types';
import { GetStaticPropsContext } from 'next';

type Props = {
  posts: Array<Post>;
};

const Blog: FC<Props> = ({ posts }) => {
  return (
    <Pane>
      <header>
        <HomeNav />
      </header>
      <main>
        <Container>
          {posts.map((post) => (
            <Pane key={post.title} marginY={majorScale(5)}>
              <PostPreview post={post} />
            </Pane>
          ))}
        </Container>
      </main>
    </Pane>
  );
};

export const getStaticProps = (ctx: GetStaticPropsContext) => {
  const cmsPosts = (ctx.preview ? postsFromCMS.draft : postsFromCMS.published).map((post) => {
    const { data } = matter(post);
    return data;
  });

  const postsPath = path.join(process.cwd(), 'posts');
  const fileNames = fs.readdirSync(postsPath);
  const filePosts = fileNames.map((file) => {
    const fullPath = path.join(process.cwd(), 'posts', file);
    const fileContent = fs.readFileSync(fullPath, 'utf-8');
    const { data } = matter(fileContent);
    return data;
  });

  const posts = [...cmsPosts, ...filePosts];

  return {
    props: { posts }
  };
};

export default Blog;
