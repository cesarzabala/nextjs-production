/* eslint-disable no-unused-vars */
import { NextApiRequest, NextApiResponse } from 'next';

export default async function onError(error: any, req: NextApiRequest, res: NextApiResponse, next: any) {
  console.log(error);
  res.status(500).end();
}
