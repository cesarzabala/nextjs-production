import { Db, MongoClient } from 'mongodb';
import { NextApiRequest, NextApiResponse } from 'next';
import ConnectToDB from '../lib/mongodb';

interface DatabaseRequest extends NextApiRequest {
  db: Db;
  dbClient: MongoClient;
}

export default async function database(req: DatabaseRequest, res: NextApiResponse, next: any) {
  const { db, dbClient } = await ConnectToDB();
  req.db = db;
  req.dbClient = dbClient;

  next();
}
