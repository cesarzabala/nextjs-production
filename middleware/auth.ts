import { NextApiRequest, NextApiResponse } from 'next';
import { getToken } from 'next-auth/jwt';

interface Request extends NextApiRequest {
  user: any;
}

export default async (req: Request, res: NextApiResponse, next: any) => {
  const token = await getToken({ req, secret: process.env.JWT_SECRET });

  if (token) {
    // Signed in
    req.user = token;
    next();
  } else {
    // Not Signed in
    res.status(401);
    res.end();
  }
};
