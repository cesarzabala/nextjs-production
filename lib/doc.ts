import { Db } from 'mongodb';
import { nanoid } from 'nanoid';

const COLLECTION = 'docs';

export const getOneDoc = async (db: Db, id: string) => {
  return db.collection(COLLECTION).findOne({ idx: id });
};

export const getDocsByFolder = async (db: Db, folderId: string) => {
  return db
    .collection(COLLECTION)
    .find({ folder: folderId }, { projection: { _id: 0 } })
    .toArray();
};

export const createDoc = async (db: Db, doc: { createdBy: string; folder: string; name: string; content?: any }) => {
  return db
    .collection(COLLECTION)
    .insertOne({
      idx: nanoid(5),
      ...doc,
      createdAt: new Date().toDateString()
    })
    .then(async ({ insertedId }) => await db.collection(COLLECTION).findOne({ _id: insertedId }));
};

export const updateOne = async (db: Db, id: string, updates: any) => {
  const operation = await db.collection(COLLECTION).updateOne(
    {
      idx: id
    },
    { $set: updates }
  );

  if (!operation.acknowledged) {
    throw new Error('Could not update document');
  }

  const updated = await db.collection('docs').findOne({ idx: id }, { projection: { _id: 0 } });
  return updated;
};
