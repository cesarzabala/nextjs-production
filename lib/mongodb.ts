/* eslint-disable no-unused-vars */
import { Db, MongoClient } from 'mongodb';

declare global {
  var mongo: {
    client: MongoClient;
  };
}

export {};

/**
 * We have to cache the DB connection
 * when used in a serverless environment otherwise
 * we may encounter performance loss due to
 * time to connect. Also, depending on your DB,
 * you might night be able to have many concurrent
 * DB connections. Most traditional DBs were not made for a stateless
 * environment like serverless. A serverless DB (HTTP based DB) whould work
 * better.
 */
global.mongo = global.mongo || {};

export type ReturnDBType = {
  db: Db;
  dbClient: MongoClient;
};

const connectToDB = async (): Promise<ReturnDBType> => {
  const uri = process.env.DATABASE_URL || '';
  if (!global.mongo.client) {
    global.mongo.client = new MongoClient(uri, {
      connectTimeoutMS: 10000
    });

    console.log('connecting to DB');
    await global.mongo.client.connect();
    console.log('connected to DB');
  }

  const db: Db = global.mongo.client.db('knownn');

  return { db, dbClient: global.mongo.client };
};

export const connectDb = async (): Promise<MongoClient> => {
  const uri = process.env.DATABASE_URL || '';
  if (!global.mongo.client) {
    global.mongo.client = new MongoClient(uri, {
      connectTimeoutMS: 10000
    });
  }

  return global.mongo.client.connect();
};

export default connectToDB;
