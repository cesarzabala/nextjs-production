import { Db } from 'mongodb';
import { nanoid } from 'nanoid';

const COLLECTION = 'folders';

export const createFolder = async (db: Db, folder: { createdBy: string; name: string }) => {
  return db
    .collection(COLLECTION)
    .insertOne({
      idx: nanoid(5),
      ...folder,
      createdAt: new Date().toDateString()
    })
    .then(async ({ insertedId }) => await db.collection(COLLECTION).findOne({ _id: insertedId }));
};

export const getFolders = async (db: Db, userId: string) => {
  return db
    .collection(COLLECTION)
    .find(
      {
        createdBy: userId
      },
      { projection: { _id: 0 } }
    )
    .toArray();
};
