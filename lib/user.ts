import { Db } from 'mongodb';

const COLLECTION = 'users';

export const getUserById = async (db: Db, id: string) => {
  return db.collection(COLLECTION).findOne({ idx: id });
};
